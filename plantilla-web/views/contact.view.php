<?php include __DIR__.'/partials/inicio-doc-part.php'; ?>

<!-- Navigation Bar -->
<?php include __DIR__.'/partials/nav.part.php'; ?>
<!-- End of Navigation Bar -->

<!-- Principal Content Start -->
   <div id="contact">
   	  <div class="container">
   	    <div class="col-xs-12 col-sm-8 col-sm-push-2">
       	   <h1>CONTACTA CON NOSOTROS</h1>
       	   <hr>
       	   <p>Escribe con detalle tus datos de contacto.</p>
           
           <form method="post" class="form-horizontal">
	       	  <div class="form-group">
	       	  	<div class="col-xs-6">
	       	  	    <label class="label-control">Nombre</label>
                            <input name="nombre" class="form-control" type="text" >
	       	  	</div>
	       	  	<div class="col-xs-6">
	       	  	    <label class="label-control">Apellido 1</label>
                            <input name="apellido" class="form-control" type="text" >
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control">E-mail</label>
                                <input name="email" class="form-control" type="text" >
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control">Asunto</label>
                                <input name="asunto" class="form-control" type="text" >
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control">Mensaje</label>
                                <textarea name="mensaje" class="form-control"></textarea>
	       	  		<button class="pull-right btn btn-lg sr-button">ENVIAR</button>
	       	  	</div>
                      
                      <!-- Aquí se incluira los valores del formulario -->
                                
                                <div>
                                    
                                    <?php
                                    
                                    //Si hay algun error lo muestro.
                                    
                                    if ($huboError === TRUE){
                                        
                                        echo "<strong>" . implode("<br/>", $errores) . "</strong>";
                                        
                                    }else{ //Si no lo hay, muestro los valores.
                                        
                                        echo "<strong>" . implode("<br/>", $datos) . "</strong>";
                                        
                                    }
                                    
                                     
                                            
                                            
                                    ?>
                                
                                </div>
                      
	       	  </div>
	       </form>
	       <hr class="divider">
	       <div class="address">
	           <h3>Contacta con nosotros</h3>
	           <hr>
	           <p>Si tienes alguna duda preguntanos:</p>
		       <div class="ending text-center">
			        <ul class="list-inline social-buttons">
			            <li><a href="#"><i class="fa fa-facebook sr-icons"></i></a>
			            </li>
			            <li><a href="#"><i class="fa fa-twitter sr-icons"></i></a>
			            </li>
			            <li><a href="#"><i class="fa fa-google-plus sr-icons"></i></a>
			            </li>
			        </ul>
				    <ul class="list-inline contact">
				       <li class="footer-number"><i class="fa fa-phone sr-icons"></i>  (00228)92229954 </li>
				       <li><i class="fa fa-envelope sr-icons"></i>  kouvenceslas93@gmail.com</li>
				    </ul>
				    <p>Photography Fanatic Template &copy; 2017</p>
		       </div>
	       </div>
	    </div>   
   	  </div>
   </div>
<!-- Principal Content Start -->

<?php include __DIR__.'/partials/fin-doc-part.php'; ?>
