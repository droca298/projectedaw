<?php

$errores = array("Lista de errores:");
$huboError = FALSE;
$datos = array("Lista de datos de contacto:");

if ($_SERVER['REQUEST_METHOD'] === 'POST'){
    
    //Comprobamos el campo nombre
    
    if(empty($_POST['nombre'])){
        
        //Ponemos el error en array de errores.
        array_push($errores, "En el campo \"Nombre\" no se encontró ningún elemento.");
        $huboError = TRUE; 
        
    }else{
        
        $nombre = trim(htmlspecialchars($_POST['nombre']));
        array_push($datos, $nombre);
        
    }
    
    //Comprobamos el campo apellidos
    
    if(empty($_POST['apellido'])){
        
        array_push($errores, "En el campo \"Apellido\" no se encontró ningún elemento."); 
        
    }else{
        
        $apellido = trim(htmlspecialchars($_POST['apellido']));
        array_push($datos, $apellido);
        
    }
    
    //Comprobamos el campo e-mail
    
    if(empty($_POST['email'])){
        
        array_push($errores, "En el campo \"E-Mail\" no se encontró ningún elemento.");
        $huboError = TRUE; 
        
    }else{
        
        
        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === FALSE){
            
            $huboError = TRUE;
            array_push($errores, "El email no es valido.<br/>");
            
        }else{
            
            $email = $_POST['email'];
            array_push($datos, $email);
            
        }
        
        
    }
    
    //Comprobamos el campo asunto.
    
    if(empty($_POST['asunto'])){
        
        //Ponemos el error en array de errores.
        array_push($errores, "En el campo \"Asunto\" no se encontró ningún elemento.");
        $huboError = TRUE; 
        
    }else{
        
        $asunto = trim(htmlspecialchars($_POST['asunto']));
        array_push($datos, $asunto);
        
    }
    
    
    
    // El mensaje no se comprueba por lo si hay mensaje o no, se mirara.
    
    if (!empty ($_POST['mensaje'])){
        
        $mensaje = $_POST['mensaje'];
        array_push($datos, $mensaje);
        
    }
    
    
}

//echo ;


require_once('views/contact.view.php');